package com.devcamp.customerinvoiceapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.models.Customer;
import com.devcamp.customerinvoiceapi.models.Invoice;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoicesRestAPI {
    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoice(){
        Customer customer1 = new Customer(1, "Lê A", 10);
        Customer customer2 = new Customer(2, "Nguyen B", 0);
        Customer customer3 = new Customer(3, "Minh", 10);
    
        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Invoice invoice1 = new Invoice(1, customer1, 10000);
        Invoice invoice2 = new Invoice(2, customer2, 10000);
        Invoice invoice3 = new Invoice(3, customer3, 7000);
        
        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);
        
        ArrayList<Invoice> arrListInvoice = new ArrayList<Invoice>();

        arrListInvoice.add(invoice1);
        arrListInvoice.add(invoice2);
        arrListInvoice.add(invoice3);

        return arrListInvoice;

    }

}
